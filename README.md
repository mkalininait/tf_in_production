# Models in production with TensorFlow-Serving
This repository contains:
1. Resources for learning how to service ML models with `TensorFlow-Serving`.
1. Pre-trained weights for a Keras implementation of [NasNet](https://arxiv.org/pdf/1707.07012.pdf), and step-by-step demo on how to deploy using the [tensorflow/serving:1.12.0 Docker image](https://hub.docker.com/r/tensorflow/serving).  
1. Pre-trained weights for a Keras implementation of [YOLO3], and step-by-step demo on how to deploy using the [tensorflow/serving:1.13.0 Docker image](https://hub.docker.com/r/tensorflow/serving).  

Clone the repo:
```
git clone https://gitlab.com/mkalininait/tf_in_production && cd tf_in_production
```

### Running in python environment

Install the required python libraries with:
```bash
pip install -r requirements.txt
```
Then start one of two notebooks (demo or yolov3):
```bash
jupyter notebook <nb_name>.ipynb
```

### Running in Docker

If you rather avoid changes to your python environment, you can run the notebook
using a Docker container. Build the docker image with:
```bash
docker build -t tf_serving_demo .
```

Then start the container and follow the instructions to open the `jupyter` server running in your browser:
```
docker run -it -p 8888:8888 -p 8500:8500 tf_serving_demo:latest
```

**NOTE**: It is a [known issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/33995) of GitLab that images embedded in a `jupyter notebook` through markup do not render properly. To properly visualize the notebook use any of the above running options.
