FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y --no-install-recommends python3 python3-pip python3-setuptools

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install --ignore-installed grpcio==1.17.0

RUN pip3 --no-cache-dir install \
        Pillow \
        h5py \
        ipykernel \
        jupyter \
        matplotlib \
        numpy \
        pandas \
        scipy \
        sklearn \
        tensorflow-serving-api==1.12.0 \
        && \
python3 -m ipykernel.kernelspec

COPY jupyter_setup/jupyter_notebook_config.py /root/.jupyter/
COPY jupyter_setup/run_jupyter.sh /

RUN mkdir /export

RUN mkdir -p /notebooks
COPY demo.ipynb /notebooks
COPY resources /notebooks/resources
COPY images /notebooks/images

RUN mkdir -p /root/.keras/
COPY models /root/.keras/models


EXPOSE 6006 8888 8500 8501

WORKDIR "/notebooks"
CMD ["/run_jupyter.sh", "--allow-root"]
